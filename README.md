# Clock for ThingPulse ESPaper

This is a basic, possibly inaccurate clock application for the [ThingPulse ESPaper](https://thingpulse.com/product/espaper-plus-kit-wifi-epaper-display/).

The timing is based on the ESP8266 deep sleep mode and a refresh via NTP every few hours. It's not guaranteed to be accurate (observed to be up to 15 seconds off), and might not handle daylight saving time correctly.

## Dependencies

- [Mini Grafx](https://github.com/ThingPulse/minigrafx)
- [Time](https://www.pjrc.com/teensy/td_libs_Time.html)
- 