
/* MIT License
 * 
 * Copyright (c) 2019 Timo Würsch
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* Includes */
#include <EPD_WaveShare.h>
#include <MiniGrafxFonts.h>
#include <Carousel.h>
#include <MiniGrafx.h>
#include <Arduino.h>
#include <SPI.h>
#include <ESP8266WiFi.h>
#include "RobotoBold52.h"
#include "RobotoLight24.h"
#include "RobotoBold16.h"
#include "RobotoLight12.h"
#include <TimeLib.h>
#include "settings.h"

/* Definitions, settings, variables */
// Pins and stuff
#define CS 15  // D8
#define RST 2  // D4
#define DC 5   // D1
#define BUSY 4 // D2
#define USR_BTN 12 // D6
#define RTC_MEMORY_USER_DATA_OFFSET 64

// Time
time_t t; // seconds
time_t last_ntp_refresh; // seconds
int last_t_minutes; // minutes that were displayed last
const unsigned long WAKEUP_INTERVAL_SECONDS = 15L;
const unsigned long NTP_REFRESH_INTERVAL_SECONDS = 6L * 3600L;
const unsigned long NTP_REFRESH_TIMEOUT_SECONDS = 3L;
#define UTC_OFFSET + 1
String WEEKDAYS[] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
String MONTHS[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

// Debug
#define DEBUG_DISPLAY 0 // On Display

// MiniGrafx definitions
#define MINI_BLACK 0
#define MINI_WHITE 1
#define SCREEN_HEIGHT 128
#define SCREEN_WIDTH 296
#define BITS_PER_PIXEL 1
uint16_t palette[] = {ILI9341_BLACK, // 0
                      ILI9341_WHITE, // 1
                     };
EPD_WaveShare epd(EPD2_9, CS, RST, DC, BUSY);
MiniGrafx graphics = MiniGrafx(&epd, BITS_PER_PIXEL, palette);

// Others
struct RTCData {
  time_t t;
  time_t last_ntp_refresh;
  int last_t_minutes;
  int rtc_memory_initialized_magic;
} rtcdata ;
const int RTC_MEMORY_INITIALIZED_MAGIC = 12345;
char str_buffer[50];

boolean connectWifi() {
  if (WiFi.status() == WL_CONNECTED) { 
    return true;
  }
  Serial.print("Connecting to wifi " + WIFI_SSID + " with password " + WIFI_PASSWORD + " ");
  WiFi.begin(WIFI_SSID.c_str(), WIFI_PASSWORD.c_str());
  int wait_count = 0;
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
    wait_count++;
    if (wait_count > 10) {
      Serial.println("Could not connect to WiFi");
      return false;
    }
  }
  Serial.println(" Connected.");
  return true;
}

void setup() {

  // Start measuring time
  unsigned long setup_begin_time = millis();

  // Start serial connection
  Serial.begin(115200);
  Serial.println("Waking up...");

  // Get reset reason
  rst_info* resetInfo = ESP.getResetInfoPtr();
  unsigned int resetReason = resetInfo->reason;

  // Get data from RTC
  // (See esp8266.ru/download/esp8266-doc/april-2015/2C-ESP8266__SDK__Programming%20Guide__EN_v1.0.1.pdf)
  system_rtc_mem_read(RTC_MEMORY_USER_DATA_OFFSET, &rtcdata, sizeof(rtcdata));
  if (rtcdata.rtc_memory_initialized_magic == RTC_MEMORY_INITIALIZED_MAGIC) {
    // RTC data is valid, use it
    t = rtcdata.t;
    last_ntp_refresh = rtcdata.last_ntp_refresh;
    last_t_minutes = rtcdata.last_t_minutes;
  } else {
    // RTC data is invalid, initialize 
    t = 0;
    last_ntp_refresh = 0;
    last_t_minutes = 0;
  }

  // Read button
  pinMode(USR_BTN, INPUT_PULLUP);
  int btnState = digitalRead(USR_BTN);

  // Check whether we need to read time from NTP
  if (t == 0 || t + WAKEUP_INTERVAL_SECONDS >= last_ntp_refresh + NTP_REFRESH_INTERVAL_SECONDS || btnState == LOW || resetReason == REASON_DEFAULT_RST) {
    if (t == 0) {
      Serial.print("Time not initialized. ");
    } else if (t + WAKEUP_INTERVAL_SECONDS >= last_ntp_refresh + NTP_REFRESH_INTERVAL_SECONDS) {
      Serial.print("NTP refresh interval. time now ");
      Serial.print(t + WAKEUP_INTERVAL_SECONDS);
      Serial.print(" next ntp refresh at ");
      Serial.print(last_ntp_refresh + NTP_REFRESH_INTERVAL_SECONDS);
    } else if (btnState == LOW) {
      Serial.print("User requested NTP refresh. ");
    } else if (resetReason == REASON_DEFAULT_RST) {
      Serial.print("Power On. ");
    }
    Serial.println("Getting time from NTP...");
    
    // Yes, so connect WiFi
    boolean wifi_connected = connectWifi();

    // Get time from NTP server
    configTime(UTC_OFFSET * 3600, 0, "0.ch.pool.ntp.org", "1.ch.pool.ntp.org", "2.ch.pool.ntp.org");
    delay(NTP_REFRESH_TIMEOUT_SECONDS * 1000L);
    t = time(NULL);
    last_ntp_refresh = t;
  } else {
    // Keep track of time manually, instead of updating from NTP on every wakeup
    t = t + WAKEUP_INTERVAL_SECONDS;
    Serial.println("Not updating from NTP.");
  }

  // Set the time for TimeLib
  setTime(t);
  Serial.printf("The time is now %02d:%02d:%02d", hour(t), minute(t), second(t));
  Serial.println("");
  
  // Initialize graphics
  graphics.init();
  graphics.setRotation(1);
  graphics.setFastRefresh(false);

  // Draw time only if the minutes have changed
  if (last_t_minutes != minute(t)) {
    drawScreen();
  }

  // Store everything in RTC memory
  Serial.print("Saving to RTC: t = ");
  Serial.print(t);
  Serial.print(" last_ntp_refresh = ");
  Serial.println(last_ntp_refresh);
  rtcdata.t = t;
  rtcdata.last_ntp_refresh = last_ntp_refresh;
  rtcdata.last_t_minutes = last_t_minutes;
  rtcdata.rtc_memory_initialized_magic = RTC_MEMORY_INITIALIZED_MAGIC;
  system_rtc_mem_write(RTC_MEMORY_USER_DATA_OFFSET, &rtcdata, sizeof(rtcdata));

  // Sleep till next update
  Serial.print("Going to deep sleep for ");
  unsigned long setup_elapsed_time = millis() - setup_begin_time;
  unsigned long sleep_time_milliseconds = WAKEUP_INTERVAL_SECONDS * 1000L - setup_elapsed_time;
  Serial.print(sleep_time_milliseconds);
  Serial.println(" ms");
  ESP.deepSleep(sleep_time_milliseconds * 1000L);
}

void loop() {
  // No loop here, everything done by deep sleep and waking up.
}

void drawScreen() {
  graphics.fillBuffer(MINI_WHITE);
  graphics.setColor(MINI_BLACK);

  sprintf(str_buffer, "%d:%02d", hour(t), minute(t));
  graphics.setTextAlignment(TEXT_ALIGN_CENTER);
  graphics.setFont(Roboto_Bold_52);
  graphics.drawString(SCREEN_WIDTH / 2, 15, String(str_buffer));
  
  graphics.setFont(Roboto_Light_24);
  sprintf(str_buffer, "%s %d %s %d", WEEKDAYS[weekday(t)-1].c_str(), day(t), MONTHS[month(t)-1].c_str(), year(t));
  graphics.drawString(SCREEN_WIDTH / 2, 85, String(str_buffer));

  if (isBatteryLow()) {
    graphics.setFont(Roboto_Light_12);
    graphics.drawString(270, 85, "battery\nlow");
  }

#if DEBUG_DISPLAY == 1

  // Debug display for time status
  graphics.setTextAlignment(TEXT_ALIGN_LEFT);
  graphics.setFont(Roboto_Bold_16);
  if (timeStatus() == timeNotSet) {
    graphics.drawString(0, 0, "timeNotSet");
  } else if (timeStatus() == timeSet) {
    graphics.drawString(0, 0, "timeSet");
  } else if (timeStatus() == timeNeedsSync) {
    graphics.drawString(0, 0, "timeNeedsSync");
  }

  // Debug display for now(), millis(), t
  graphics.setTextAlignment(TEXT_ALIGN_RIGHT);
  sprintf(str_buffer, "now() %d", now());
  graphics.drawString(SCREEN_WIDTH, 0, str_buffer);
  sprintf(str_buffer, "millis() %d", millis());
  graphics.drawString(SCREEN_WIDTH, SCREEN_HEIGHT-20, str_buffer);
  graphics.setTextAlignment(TEXT_ALIGN_LEFT);
  sprintf(str_buffer, "t %d", t);
  graphics.drawString(0, SCREEN_HEIGHT-20, str_buffer);

  // Debug display for button state
  pinMode(USR_BTN, INPUT_PULLUP);
  int btnState = digitalRead(USR_BTN);
  sprintf(str_buffer, "btnState %d", btnState);
  graphics.setTextAlignment(TEXT_ALIGN_CENTER);
  graphics.drawString(SCREEN_WIDTH / 2, SCREEN_HEIGHT-20, str_buffer);
  
#endif

  last_t_minutes = minute(t);
  graphics.commit();
}

bool isBatteryLow() {
  // Taken straight from espaper-weatherstation:
  // https://github.com/ThingPulse/espaper-weatherstation/blob/f2ee0624ce95f571d995de09a2fbf9382b156e16/espaper-weatherstation.ino#L418
  
  uint8_t percentage = 100;
  float adcVoltage = analogRead(A0) / 1024.0;
  float batteryVoltage = adcVoltage * 4.945945946 -0.3957657658;
  if (batteryVoltage > 4.2) percentage = 100;
  else if (batteryVoltage < 3.3) percentage = 0;
  else percentage = (batteryVoltage - 3.3) * 100 / (4.2 - 3.3);
  return (percentage <= 15);
}

